import React from 'react';

const ThankYou = () => {
    return (
        <div className="ty">
            Thank you for completing the survey!
        </div>
    );
};

export default ThankYou;