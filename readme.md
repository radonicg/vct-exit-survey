# VCT Survey component

This component can be used for any survey that has multiselect and/or textearea question types.
Component uses two props:
- endpoint
- data

Endpoint is a string which represents submit endpoint, e.g. 'https://appdomain.com/api/submit'.
Data is a Javascript object that represents questions. One example of data object is here:

```
{
    description: 'Description of survey. For example: Thank you for using this application. Would you provide us your thoughts?',
    questions: [
        {
            question: 'First question?',
            type: 'multiselect',
            options: {
                'Answer 1': false,
                'Answer 2': false,
                'Answer 3': false,
            }
        },
        {
            question: 'Second question?',
            type: 'textarea'
        }
    ]
}

```

For now, the component supports only textarea and multiselect question types. In case of multiselect question, boolean value indicates wheater the answer should be selected initially or not.