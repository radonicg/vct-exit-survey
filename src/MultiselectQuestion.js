import React, { useState } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const MultiselectQuestion = (props) => {
    const [options, setOptions] = useState(props.options);

    const handleChange = ({ name, checked: value }) => {
        let newState = { ...options, ...{ [name]: value } };
        setOptions(newState);
        props.parentHandler(props.id, newState);
    }
    return (
        <div>
            <span className="question">{props.question}</span>
            <ul>
                {Object.entries(options).map(([name, checked]) => {
                    return <li key={name}><FormControlLabel control={<Checkbox name={name} checked={checked} onChange={(e) => handleChange(e.target)} size='small' />} label={name} /></li>
                })}
            </ul>
        </div>
    );
};

export default MultiselectQuestion;