import React, { useState } from 'react';
import MultiselectQuestion from './MultiselectQuestion';
import TextareaQuestion from './TextareaQuestion';
import Button from '@material-ui/core/Button';

import ThankYou from './ThankYou';
import './survey.css';

const Survey = ({ data, endpoint }) => {

    const [message, setMessage] = useState('');
    const [surveyState, setSurveyState] = useState({});
    const [submitted, setSubmitted] = useState(false);


    const handler = (key, value) => {
        setSurveyState({ ...surveyState, ...{ [key]: typeof value === 'object' ? Object.keys(value).filter(key => value[key] === true) : value } });
    }
    const submitSurvey = async (url = '', data = {}) => {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return response;
    }

    const handleSubmit = async () => {
        let result = Object.keys(surveyState)
            .filter(key => surveyState[key].length > 0)
            .reduce((res, key) => (res[key] = surveyState[key], res), {});
        if (Object.keys(result).length === 0) {
            setMessage('Looks like this form is empty. Try filling it out before submitting.');
        } else {
            console.log(result);
            let response = await submitSurvey(endpoint, {
                data: result
            });
            if (response.status === 200) {
                setMessage('Thank you for completing the survey!');
                setSurveyState({});
                setSubmitted(true);
            } else {
                setMessage('Something went wrong. Try again.');
            }
        }
    }

    const renderQuestions = () => {
        return data.questions.map((el, id) => {
            switch (el.type) {
                case 'multiselect':
                    return <MultiselectQuestion key={id} id={id} question={el.question} options={el.options} parentHandler={handler} />
                case 'textarea':
                    return <TextareaQuestion key={id} id={id} question={el.question} parentHandler={handler} />
            }
        })
    }
    return (
        <div>
            {!submitted &&
                <div>
                    <p>{data.description}</p>
                    <div className="survey">
                        {renderQuestions()}
                        <Button className="submit" variant="outlined" color="secondary" onClick={handleSubmit}>Submit</Button>
                        <span className="info">{message}</span>
                    </div>
                </div>
            }
            {submitted &&
                <div className="survey">
                    <ThankYou />
                </div>
            }
        </div>
    );
};

export default Survey;