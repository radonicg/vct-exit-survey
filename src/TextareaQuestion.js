import React, { useState } from 'react';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

const TextareaQuestion = (props) => {
    const [textarea, setTextarea] = useState('');

    const handleChange = (data) => {
        setTextarea(data.value);
        props.parentHandler(props.id, data.value);
    }
    return (
        <div>
            <span className="question">{props.question}</span>
            <TextareaAutosize rowsMin="5" value={textarea} onChange={(e) => handleChange(e.target)} />
        </div>
    );
};

export default TextareaQuestion;